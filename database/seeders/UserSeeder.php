<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    const USERS = [
        [
            'second_name' => 'Распутная',
            'first_name' => 'Анна',
        ],
        [
            'second_name' => 'Маковей',
            'first_name' => 'Никита',
        ],
        [
            'second_name' => 'Лоншакова',
            'first_name' => 'Анастасия',
        ],
        [
            'second_name' => 'Романенкова',
            'first_name' => 'Людмила',
        ],
        [
            'second_name' => 'Ланцов',
            'first_name' => 'Игорь',
        ],
        [
            'second_name' => 'Костенков',
            'first_name' => 'Владислав',
        ],
        [
            'second_name' => 'Тимофеев',
            'first_name' => 'Андрей',
        ],
        [
            'second_name' => 'Салушкин',
            'first_name' => 'Дмитрий',
        ],
        [
            'second_name' => 'Зверева',
            'first_name' => 'Дарья',
        ],
        [
            'second_name' => 'Гасанова',
            'first_name' => 'Сабина',
        ],
        [
            'second_name' => 'Снетков',
            'first_name' => 'Данила',
        ],
        [
            'second_name' => 'Ярушина',
            'first_name' => 'Мария',
        ],
        [
            'second_name' => 'Катков',
            'first_name' => 'Александр',
        ],
    ];

    public function run()
    {
        User::factory(count(self::USERS))->create();

        foreach (User::all() as $key => $user) {
            $user->update(self::USERS[$key]);
        }
    }
}
