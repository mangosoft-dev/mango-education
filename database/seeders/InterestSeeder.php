<?php

namespace Database\Seeders;

use App\Models\Interest;
use Illuminate\Database\Seeder;

class InterestSeeder extends Seeder
{
    const INTERESTS = [
        'Языки',
        'Математика',
        'Программирование',
        'Спорт',
        'Искусство',
        'Музыка',
        'Экология',
        'Лыжи',
        'Физика',
        'Робототехника',
        'Путешествия',
        'Дебаты',
        'История',
        'Реконструкция',
        'Танцы',
        'Киберспорт',
        'Настольные игры',
        'Химия',
        'Экономика',
        'Книги',
        'Поэзия',
        'Живопись',
        'Туризм',
        'Дискотеки',
        'Кулинария',
    ];

    public function run()
    {
        Interest::factory(count(self::INTERESTS))->create();

        foreach (Interest::all() as $key => $interest) {
            $interest->update(['name' => self::INTERESTS[$key]]);
        }
    }
}
