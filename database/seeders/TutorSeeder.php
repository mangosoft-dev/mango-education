<?php

namespace Database\Seeders;

use App\Models\Tutor;
use App\Models\User;
use Illuminate\Database\Seeder;

class TutorSeeder extends Seeder
{
    public function run()
    {
        $users = User::query()->limit(13)->get();
        foreach ($users as $user) {
            Tutor::create(['user_id' => $user->id]);
        }
    }
}
