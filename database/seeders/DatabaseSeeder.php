<?php

namespace Database\Seeders;

use App\Models\Cell;
use App\Models\Interest;
use App\Models\Subject;
use App\Models\Tutor;
use App\Models\User;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    const SUBJECTS = [
        [
            'name' => 'Английский язык',
            'description' => 'Помогу расширить словарный запас, научу говорить, как носитель, расскажу, чем present simple отличается от present perfect',
        ],
        [
            'name' => 'География',
            'description' => 'Покажу, где находится Сочи и как до него добраться. Расскажу про Гваделупу и тех, кто там живет',
        ],
        [
            'name' => 'Физика',
            'description' => 'Со мной ты узнаешь, почему в машине потеют окна от дождя и алкоголя. Я расскажу закон Ньютона так, как не рассказывал никто до меня',
        ],
        [
            'name' => 'Биология',
            'description' => 'Мы изучим мартышек, горилл, шимпанзе и человека. Узнаешь, чем инфузория туфелька отличается от твоей подружки.',
        ],
        [
            'name' => 'Математический анализ',
            'description' => 'Научу тебя разбираться в том, в чем не разбираются даже твои родители.',
        ],
        [
            'name' => 'Дифференциальные уравнения',
            'description' => 'Я буду рассказывать так, что тебе не придется после пар просить помощи у Павла Максимова',
        ],
        [
            'name' => 'Танцы',
            'description' => 'Научу тебя двигаться так, что тебе будет завидовать даже Маринка из параллельной группы',
        ],
        [
            'name' => 'Экономика',
            'description' => 'Научу тебя тратить деньги мужа так, чтобы он ничего не заподозрил',
        ],
        [
            'name' => 'Кулинария',
            'description' => 'Научу тебя готовить так, чтобы тебе не приходилось идти к репетитору по Экономике',
        ],
        [
            'name' => 'Химия',
            'description' => 'Покажу, как обесцветить волосы в домашних условиях без вреда и как найти алкоголь, от которого ты не ослепнешь',
        ],
        [
            'name' => 'Киберспорт',
            'description' => 'Научу тебя играть в Dota так, чтобы вражеский мидер заплакал, а гангеры разбегались в страхе',
        ],
        [
            'name' => 'Программирование',
            'description' => 'Покажу тебе, как написать искусственный интеллект, используя только html, css и pascal',
        ],
        [
            'name' => 'Русский язык',
            'description' => 'Покажу тебе великий Русский язык, расскажу, как оскорбить своих недругов так, чтобы они этого не поняли',
        ]
    ];

    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(InterestSeeder::class);

        $users = User::all()->pluck('id');
        $interests = Interest::all()->pluck('id');
        foreach ($users as $user) {
            $separatedValue = time() % 10;
            foreach ($interests as $interest) {
                if ($interest % $separatedValue === 0) {
                    DB::table('interest_user')->insert([
                        'user_id' => $user,
                        'interest_id' => $interest
                    ]);
                }
            }
        }


        $this->call(TutorSeeder::class);
        $this->call(SubjectSeeder::class);

        $tutors = Tutor::all()->pluck('id');
        $subjects = Subject::all()->pluck('id');
        foreach ($tutors as $tutor) {
            $separatedValue = rand(0, count(self::SUBJECTS)) % count(self::SUBJECTS) + 1;
            foreach ($subjects as $subject) {
                if ($subject % $separatedValue === 0) {
                    $subjectName = Subject::find($subject)->name;
                    $subjectDescription = null;
                    foreach (self::SUBJECTS as $item) {
                        if ($subjectName == $item['name']) {
                            $subjectDescription = $item['description'];
                        }
                    }
                    DB::table('subject_tutor')->insert([
                        'tutor_id' => $tutor,
                        'subject_id' => $subject,
                        'description' => $subjectDescription,
                    ]);
                }
            }
        }

        $this->call(StorySeeder::class);
        $this->call(CellSeeder::class);

        foreach ($tutors as $tutor) {
            $cellsLimit = time() % 10 + 2;
            for ($i = 0; $i < $cellsLimit; $i++) {
                Cell::create([
                    'tutor_id' => $tutor,
                    'start' => Carbon::parse(now(null))->addDays($i)->addHours($i + 1)->format('Y/m/d H:i'),
                    'end' => Carbon::parse(now(null))->addDays($i)->addHours($i + 3)->format('Y/m/d H:i'),
                    'description' => \Faker\Factory::create()->numberBetween(100, 2000),
                    'title' => Subject::find(\Faker\Factory::create()->randomElement(
                        DB::table('subject_tutor')->where(['tutor_id' => $tutor])->get()->pluck('subject_id')->toArray()
                    ))->name,
                ]);
            }
        }
    }
}
