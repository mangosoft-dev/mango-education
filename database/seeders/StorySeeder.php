<?php

namespace Database\Seeders;

use App\Models\Story;
use Illuminate\Database\Seeder;

class StorySeeder extends Seeder
{
    const STORIES = [
        'CODE work Challenge',
        'Лекция про раздельный сбор мусора',
        'Вездекод',
        'Программирование для чайников',
        'Курсы кройки и шитья',
        'Собрание спелеологов',
        'Физика для гуманитариев',
        'Японский для корейцев',
    ];

    public function run()
    {
        Story::factory(8)->create();

        foreach (Story::all() as $key => $story) {
            $story->update(['title' => self::STORIES[$key]]);
        }
    }
}
