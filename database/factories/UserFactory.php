<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition()
    {
        $degree = $this->faker->randomElement(['Бакалавриат', 'Специалитет', 'Магистратура']);
        $course = null;
        switch ($degree) {
            case 'Бакалавриат':
                $course = $this->faker->randomElement([1, 2, 3, 4]);
                break;
            case 'Специалитет':
                $course = $this->faker->randomElement([1, 2, 3, 4, 5]);
                break;
            case 'Магистратура':
                $course = $this->faker->randomElement([1, 2]);
                break;
        }

        return [
            'first_name' => $this->faker->firstName,
            'second_name' => $this->faker->lastName,
            'phone_number' => $this->faker->phoneNumber,
            'email' => $this->faker->safeEmail,
            'password' => 'password',
            'degree' => $degree,
            'course' => $course,
            'specialty' => $this->faker->randomElement([
                'Прикладная математика и информатика',
                'Математика и компьютерные науки',
                'VR/AR технологии'
            ]),
            'file_path' => "/uploads/person{$this->faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])}.jpeg",
        ];
    }
}
