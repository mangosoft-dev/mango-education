<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SlotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'day' => Carbon::parse($this->start)->locale('ru')->isoFormat('dddd, Do MMMM'),
            'time' => Carbon::parse($this->start)->format('H:i') . " - " . Carbon::parse($this->end)->format('H:i'),
            'price' => $this->description,
        ];
    }
}
