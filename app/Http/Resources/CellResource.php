<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CellResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tutor_id' => $this->tutor_id,
            'description' => $this->description,
            'title' => $this->title,
            'start' => $this->start, //Carbon::parse($this->start)->format('Y/m/d H:i'),
            'end' => $this->end, //Carbon::parse($this->end)->format('Y/m/d H:i'),
        ];
    }
}
