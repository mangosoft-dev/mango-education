<?php

namespace App\Http\Resources;

use App\Models\Tutor;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        $isTutor = !!$this->tutor()->count();
        $tutor = Tutor::where(['user_id' => $this->id])->first();

        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'second_name' => $this->second_name,
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'degree' => $this->degree,
            'course' => $this->course,
            'specialty' => $this->specialty,
            'file_path' => $this->file_path ? config('app.url') . $this->file_path : null,
            'interests' => InterestResource::collection($this->interests),
            'is_tutor' => $isTutor,
            'subjects' => $isTutor ? SubjectResource::collection($tutor->subjects) : [],
        ];
    }
}
