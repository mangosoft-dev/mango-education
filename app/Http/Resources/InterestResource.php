<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InterestResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_chosen' => auth('sanctum')->user() ? auth('sanctum')->user()->interests()->where(['interests.id' => $this->id])->exists() : false,
        ];
    }
}
