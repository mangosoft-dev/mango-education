<?php

namespace App\Http\Resources;

use App\Models\Cell;
use App\Models\Subject;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class TutorResource extends JsonResource
{
    public function toArray($request)
    {
        $subject = Subject::find($this->subject_id);
        return [
            'id' => $this->id,
            'rating' => rand(5, 9) . ".0",
            'user' => new UserResource(User::find(Tutor::find($this->tutor_id)->user_id)),
            'subject' => new SubjectResource($subject),
            'description' => $this->description,
            'slots' => SlotResource::collection(Cell::query()->where([
                'tutor_id' => $this->tutor_id,
                'title' => $subject->name,
            ])->get()),
        ];
    }
}
