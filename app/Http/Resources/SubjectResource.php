<?php

namespace App\Http\Resources;

use App\Models\Tutor;
use Illuminate\Http\Resources\Json\JsonResource;

class SubjectResource extends JsonResource
{
    public function toArray($request)
    {
        $tutor = auth('sanctum')->user() ?
            Tutor::where(['user_id' => auth('sanctum')->user()->id])->first() : null;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_chosen' => $tutor ? $tutor->subjects()->where(['subjects.id' => $this->id])->exists() : false,
            'description' => $tutor && $tutor->subjects()->where(['subjects.id' => $this->id])->exists() ?
                $tutor->subjects()->where(['subjects.id' => $this->id])->first()->pivot->description : null,
        ];
    }
}
