<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CellResource;
use App\Http\Resources\TutorResource;
use App\Mail\RequestMail;
use App\Models\Cell;
use App\Models\Tutor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class TutorController extends Controller
{
    public function index()
    {
        return response()->json(TutorResource::collection(DB::table('subject_tutor')->inRandomOrder()->get()));
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function getCells(Request $request)
    {
        $user = auth('sanctum')->user();
        if ($user->tutor()->exists()) {
            return response()->json(CellResource::collection(Tutor::query()->where(['user_id' => $user->id])->first()->cells));
        }
        return response()->json([]);
    }

    public function updateCells(Request $request)
    {
        $user = auth('sanctum')->user();
        if ($user->tutor()->exists()) {
            $tutor = Tutor::query()->where(['user_id' => $user->id])->first();
            foreach ($request->get('cells', []) as $cell) {
                $cell['tutor_id'] = $tutor->id;
                Cell::create($cell);
            }
        }
        return response()->json([]);
    }

    public function submitRequest(Request $request, $id)
    {
        $cell = Cell::find($id);
        $tutor = Tutor::find($cell->tutor_id)->user;
        $data = [
            'start_date' => Carbon::parse($cell->start)->format('d/m/Y H:i'),
            'tutor_data' => "{$tutor->second_name} {$tutor->first_name} ({$tutor->email}, {$tutor->phone_number})",
            'url' => 'http://localhost:8080',
        ];

        try {
            Mail::to(auth('sanctum')->user())->send(new RequestMail($data));
        }
        catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }
}
