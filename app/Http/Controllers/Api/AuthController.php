<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
            'password' => 'required|min:6|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 403);
        }

        $user = User::where(['email' => $request->get('email')])->first();
        if (Hash::check($request->get('password'), $user->password)) {
            $token = $user->createToken('app-token')->plainTextToken;
            return response()->json(['user' => new UserResource($user)], 200)->withHeaders(['Authorization' => $token]);
        }

        return response()->json([
            'errors' => [
                'password' => [
                    'The current password does not match this user.'
                ]
            ]
        ], 403);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'second_name' => 'required|string',
            'phone_number' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|string',
            'interests' => 'nullable|array',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 403);
        }

        $user = User::create($validator->validated());
        $user->interests()->sync($request->get('interests', []));

        $token = $user->createToken('app-token')->plainTextToken;
        return response()->json(['user' => new UserResource($user)], 201)->withHeaders(['Authorization' => $token]);
    }

    public function logout(Request $request)
    {
        auth('sanctum')->user()->currentAccessToken()->delete();
        return response('', 204);
    }

    public function user(Request $request)
    {
        return response()->json(['data' => new UserResource(auth('sanctum')->user())], 201);
    }
}
