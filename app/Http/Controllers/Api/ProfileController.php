<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Tutor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function uploadImage(Request $request)
    {
        $user = auth('sanctum')->user();
        $filePath = $request->file('file')->store('uploads');
        $data = ['file_path' => "/storage/{$filePath}"];
        $user->update($data);
        return response()->json($data);
    }

    public function update(Request $request)
    {
        $user = auth('sanctum')->user();
        $user->update($request->except('file_path'));
        $user->interests()->sync($request->get('interests', []));
        if ($request->get('is_tutor')) {
            $tutor = $user->tutor()->exists() ? $user->tutor()->first() : Tutor::create(['user_id' => $user->id]);
            $tutor->subjects()->sync([]);
            foreach ($request->get('subjects', []) as $item) {
                DB::table('subject_tutor')->insert([
                    'tutor_id' => $tutor->id,
                    'subject_id' => $item["id"],
                    'description' => $item["description"]
                ]);
            }
        }
        return response()->json([]);
    }
}
