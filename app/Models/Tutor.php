<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tutor extends Model
{
    use HasFactory;

    protected $fillable = ['user_id'];

    public function subjects()
    {
        return $this->belongsToMany(Subject::class)->withPivot(['description']);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cells()
    {
        return $this->hasMany(Cell::class);
    }
}
