<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'first_name',
        'second_name',
        'phone_number',
        'email',
        'email_verified_at',
        'password',
        'degree',
        'course',
        'specialty',
        'file_path',
    ];

    protected $hidden = [
        'password',
    ];

    public function interests()
    {
        return $this->belongsToMany(Interest::class);
    }

    public function tutor()
    {
        return $this->hasMany(Tutor::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
