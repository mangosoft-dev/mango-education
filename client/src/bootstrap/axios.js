import axios from 'axios';

axios.defaults.withCredentials = true;
axios.defaults.headers.Accept = "application/json";
axios.defaults.baseURL = process.env.VUE_APP_APP_URL;
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios = axios;

export default axios
