import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import vuetify from './plugins/vuetify';
import './plugins/vue-auth'
import 'axios-progress-bar/dist/nprogress.css'
import {loadProgressBar} from 'axios-progress-bar'
import _ from 'lodash';
import "./styles/index.scss";
import { SchedulerInstaller } from '@progress/kendo-scheduler-vue-wrapper'

Object.defineProperty(Vue.prototype, '$_', { value: _ });

Vue.config.productionTip = false
require('./bootstrap');
loadProgressBar()

Vue.use(SchedulerInstaller)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
