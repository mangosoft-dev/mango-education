import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
      path: '/',
      name: 'Home',
      component: () => import(/* webpackChunkName: "Home" */ '../views/Home.vue'),
      meta: {
        auth: true
      },
    },
    {
      path: '/home',
      name: 'Index',
      component: () => import(/* webpackChunkName: "SignUp" */ '../views/SignUp.vue'),
      meta: {
        auth: false
      },
    },
    {
      path: '/account',
      name: 'Account',
      component: () => import(/* webpackChunkName: "Account" */ '../views/Account.vue'),
      meta: {
        auth: true
      },
    },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
