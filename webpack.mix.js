const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
    extractVueStyles: 'public/css/components.css',
});

mix.webpackConfig({
    resolve: {
        alias: {
            '@': __dirname + './resources',
        }
    },
})

mix
  .js('resources/js/main.js', 'public/js')
  .sass('resources/css/styles/index.scss', 'public/css')


