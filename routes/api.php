<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\{
    TutorController,
    StoryController,
    AuthController,
    InterestController,
    SubjectController,
    ProfileController,
};

Route::resource('/tutors', TutorController::class);
Route::resource('/stories', StoryController::class);
Route::resource('/interests', InterestController::class);
Route::resource('/subjects', SubjectController::class);

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/upload', [ProfileController::class, 'uploadImage']);
    Route::post('/user/update', [ProfileController::class, 'update']);
    Route::get('/requests/{id}/submit', [TutorController::class, 'submitRequest']);
    Route::get('/cells', [TutorController::class, 'getCells']);
    Route::post('/schedule/update', [TutorController::class, 'updateCells']);
    Route::get('/user', [AuthController::class, 'user']);
    Route::get('/logout', [AuthController::class, 'logout']);
});
