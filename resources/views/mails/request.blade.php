@component('mail::message')
# Занятие начнётся {{ $start_date }}!

## Данные репетитора: {{ $tutor_data }}

@component('mail::button', ['url' => $url])
Перейти на платформу
@endcomponent

Спасибо,<br>
{{ config('app.name') }}
@endcomponent